//
//  PhyloMyGamesViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloMyGamesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView* gamesTable;
}

@property (nonatomic, retain) UITableView* gamesTable;
@end
