//
//  PhyloLocationsViewController.h
//  Phylo
//
//  Created by Michael Riyanto on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface Current_LatLongViewController : UIViewController {
    CLLocationManager *locationManager;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
-(IBAction)Button:(id)sender;
@end