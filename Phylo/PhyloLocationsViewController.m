//
//  PhyloLocationsViewController.m
//  Phylo
//
//  Created by Michael Riyanto on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloLocationsViewController.h"
@implementation Current_LatLongViewController
@synthesize locationManager;
- (void)dealloc
{
    [super dealloc];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn’t have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren’t in use.
}
#pragma mark – View lifecycle
- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(IBAction)Button:(id)sender
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    // Configure the new event with information from the location
    CLLocationCoordinate2D coordinate = [location coordinate];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    NSLog(@"dLatitude : %@", latitude);
    NSLog(@"dLongitude : %@",longitude);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(50, 40, 250, 50)];
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 80, 200, 50)];
    UILabel *myLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 120, 200, 100)];
    myLabel.textColor = [UIColor blackColor];
    myLabel1.textColor = [UIColor blackColor];
    label.backgroundColor = [UIColor clearColor];
    myLabel.backgroundColor = [UIColor clearColor];
    myLabel1.backgroundColor = [UIColor clearColor];
    [myLabel setText:latitude];
    [myLabel1 setText:longitude];
    label.text = @"Current Latitude And Longitude";
    [self.view addSubview:label];
    [self.view addSubview:myLabel];
    [self.view addSubview:myLabel1];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
@end